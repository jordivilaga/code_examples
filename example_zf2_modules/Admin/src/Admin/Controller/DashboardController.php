<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

// Zend
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Placeholder controller
 *
 * This controller is just here in case you have not defined a controller
 * behind the 'admin' route yourself. If you haven't, you would otherwise
 * get a 404: Page not found error.
 *
 * If you want to override this controller (and action), create a module and
 * put this in the module configuration:
 *
 * <code>
 * <?php
 * return array(
 *     'router' => array(
 *         'routes' => array(
 *             'admin' => array(
 *                 'options' => array(
 *                     'defaults' => array(
 *                         'controller' => 'MyFoo\Controller\OtherController',
 *                         'action'     => 'custom',
 *                     ),
 *                 ),
 *             ),
 *         ),
 *     ),
 * );
 * </code>
 *
 * @package    Admin
 * @subpackage Controller
 */

class DashboardController extends AbstractActionController
{
    public function indexAction()
    {
        return array();
    }
}
