<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

// Zend
use Zend\ModuleManager\Feature;
use Zend\Loader;
use Zend\EventManager\EventInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\ModuleRouteListener;


class Module implements Feature\AutoloaderProviderInterface, Feature\ConfigProviderInterface, Feature\ServiceProviderInterface, Feature\BootstrapListenerInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'admin_navigation' => 'Admin\Navigation\Service\AdminNavigationFactory',
            ),
        );
    }

    public function onBootstrap(EventInterface $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'selectLayoutBasedOnRoute'));

        // We need to do this to override the error template to the admin layout
        $eventManager->attach("dispatch.error", function($e) {
            
            $vm = $e->getViewModel();
            $vm->setTemplate('layout/admin');

            return;
        }, 100);

        // We want to redirect always to admin area, we do not want front area
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        // Nothing's available for non logged user, so redirect him to login page
        $eventManager->attach("dispatch", function($e) {

            $sm = $e->getApplication()->getServiceManager();
            $controller = $e->getTarget();
            $auth = $sm->get('zfcuser_auth_service');
            //echo '<PRE>'; print_r($e->getRouteMatch()->getMatchedRouteName()); die;
            if (!$auth->hasIdentity() && $e->getRouteMatch()->getMatchedRouteName() !== 'zfcuser/login') {
                $application = $e->getTarget();

                $e->stopPropagation();
                $response = $e->getResponse();
                $response->setStatusCode(302);
                $response->getHeaders()->addHeaderLine('Location', $e->getRouter()->assemble(array(), array('name' => 'zfcuser/login')));
                //returning response will cause zf2 to stop further dispatch loop
                return $response;
            } else if(strpos($e->getRouteMatch()->getMatchedRouteName(), 'admin') === false && strpos($e->getRouteMatch()->getMatchedRouteName(), 'zfcuser') === false) {
                $vm = $e->getViewModel();
                $vm->setTemplate('layout/admin');

                $response = $e->getResponse();
                $response->setStatusCode(404);
                return;
            }

            /*if(strpos($e->getRouteMatch()->getMatchedRouteName(), 'admin') === false) {
                $e->stopPropagation();
                $response = $e->getResponse();
                $response->setStatusCode(302);
                $response->getHeaders()->addHeaderLine('Location', $e->getRouter()->assemble(array(), array('name' => 'admin')));
                //returning response will cause zf2 to stop further dispatch loop
                return $response;
            }*/

        }, 100);
    }

    /**
     * Select the admin layout based on route name
     *
     * @param  MvcEvent $e
     * @return void
     */
    public function selectLayoutBasedOnRoute(MvcEvent $e)
    {
        $app    = $e->getParam('application');
        $sm     = $app->getServiceManager();
        $config = $sm->get('config');

        if (false === $config['admin']['use_admin_layout']) {
            return;
        }

        $match      = $e->getRouteMatch();
        $controller = $e->getTarget();
        if (!$match instanceof RouteMatch
            || 0 !== strpos($match->getMatchedRouteName(), 'admin')
            || $controller->getEvent()->getResult()->terminate()
        ) {
            return;
        }

        $layout     = $config['admin']['admin_layout_template'];
        $controller->layout($layout);
    }
}
