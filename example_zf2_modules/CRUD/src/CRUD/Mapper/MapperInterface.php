<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Mapper;


interface MapperInterface
{
	/**
	 * Get all elements
	 *
	 * return mixed list of elements
	 */
	public function findAll();

	/**
	 * Get the paginator adapter
	 *
	 * return \Zend\Paginator\AdapterInterface
	 */
	public function getPaginationAdapter();
}