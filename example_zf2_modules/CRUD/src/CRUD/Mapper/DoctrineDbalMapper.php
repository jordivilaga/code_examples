<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Mapper;

// Doctrine
use Doctrine\ORM\EntityManager;

// Pagination (Doctrine)
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as PaginatorAdapter;

// Base
use Base\EventManager\EventProvider;

class DoctrineDbalMapper extends EventProvider implements MapperInterface
{
	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	/**
	 * @var string
	 */
	protected $entityName;

    /**
     * @var Query
     */
    protected $query;

	/**
	 * Get all elements
	 *
	 * return mixed list of elements
	 */
	public function findAll($orderBy = null, $order = null, $keyword = null)
	{

		return $this->getQuery($orderBy, $order, $keyword)->execute();
	}

    /**
     * Save an Entity
     *
     * @return Entity
     */
    public function save($entity)
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this, array('entity' => $entity));
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        $this->getEventManager()->trigger(__FUNCTION__ . 'post', $this, array('entity' => $entity));
        return $entity;
    }

    /**
     * Save an entity
     *
     * @return Entity
     */
    public function find($identifier)
    {
        return $this->getEntityManager()->getRepository($this->getEntityName())->find($identifier);
    }

    /**
     * Remove an entity
     *
     * @param Entity $entity
     */
    public function remove($entity)
    {
        $this->getEventManager()->trigger(__FUNCTION__ . '.pre', $this, array('entity' => $entity));
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
        $this->getEventManager()->trigger(__FUNCTION__ . '.post', $this, array());
    }

	/**
	 * Get the paginator adapter
	 *
	 * return \Zend\Paginator\AdapterInterface
	 */
	public function getPaginationAdapter()
	{
		return new PaginatorAdapter(new DoctrinePaginator($this->getQuery()));
	}

	/**
	 * Get query instance
	 *
	 * @return \Doctrine\ORM\Query
	 */
    protected function getQuery($orderBy, $order, $keyword = null)
    {
        if ($this->query === null) {

            $queryBuilder = $this->entityManager->createQueryBuilder();
            $queryBuilder->select('e');
            $queryBuilder->from($this->entityName, 'e');
            //$queryBuilder->where('e.name = :keyword');
            

            if($order)
            {
                $queryBuilder->orderBy('e.'.$orderBy, $order);
            }

            if($keyword)
            {
                $fields = $this->entityManager->getClassMetadata($this->entityName)->getFieldNames();
                $orx = $queryBuilder->expr()->orX();
                foreach($fields as $key => $field)
                {
                    $orx->add($queryBuilder->expr()->like('e.'.$field, ':'.$field));
                    $queryBuilder->setParameter($field, '%'.$keyword.'%');
                }
                $queryBuilder->where($orx);
            }

            $this->getEventManager()->trigger(__FUNCTION__ . 'pre', $this, array('queryBuilder' => $queryBuilder));

            $this->query = $queryBuilder->getQuery();

        }        

        return $this->query;
    }

    /**
     * Get EntityManager instance
     *
     * @return Form
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            return false;
        }
        return $this->entityManager;
    }

    /**
     * Set EntityManager instance
     *
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get Entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        if ($this->entityName === null) {
            return false;
        }
        return $this->entityName;
    }

    /**
     * Set Entity name
     *
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }
}