<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Helper\Form;


class HelperFormTabs extends HelperForm
{

    /**
     * @var boolean
     */
    //public $show_toolbar = true;

    /**
     * @var string
     */
    public $layout = 'crud/layout/form_body_tabs';

    /**
     * @var array
     */
    public $tabs = array();


    public function addTab($key, $title, $elements, $attrs)
    {
    	array_push($this->tabs, array(
    		'key' => $key,
    		'title' => $title,
    		'attrs' => $attrs,
    		'content' => array(
    			'elements' => $elements,
    		),
    	));
    }
}