<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Helper;


class HelperView
{
    /**
     * @var string
     */
    public $columns = 2;

    /**
     * @var array
     */
    public $elements = array();

    /**
     * @var boolean
     */
    //public $show_toolbar = true;

    /**
     * @var string
     */
    //public $layout = 'crud/layout/index';




    public function __construct()
    {

    }

}