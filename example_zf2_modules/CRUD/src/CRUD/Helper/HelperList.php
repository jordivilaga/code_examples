<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Helper;


class HelperList extends Helper
{

    /**
     * @var array
     */
    public $headings = array();

    /**
     * @var boolean
     */
    //public $show_toolbar = true;

    /**
     * @var string
     */
    //public $layout = 'crud/layout/index';

    /**
     * @var string
     */
    public $layout_pagination = 'crud/layout/pagination';

    /**
     * @var array
     */
    public $pagination = array(20, 50, 100, 300);

    /**
     * Default order by clouse determined by arrows in list header
     *
     * @var string
     */
    public $orderBy = 'id';

    /**
     * Default order way clouse (ASC, DESC) determined by arrows in list header
     *
     * @var string
     */
    public $orderWay = 'ASC';

    /**
     * Default page number
     *
     * @var string
     */
    public $page = 1;




    public function __construct()
    {

    }

}