<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CRUD\Controller;

 // Zend
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Form;

// CRUD
use CRUD\Mapper\MapperInterface;


abstract class Controller extends AbstractActionController
{
	/**
     * @var MapperInterface
     */
    private $mapper;

    /**
     * @var string
     */
    private $mapperName;

    /**
     * @var string
     */
    protected $formName;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $moduleName;

    /**
     * @var string
     */
    private $controllerName;

    /**
     * @var ViewModel
     */
    private $viewModel;

    /**
     * @var Form
     */
    protected $form;


    /**
     * Constructor
     */
    public function __construct()
    {
    	$class = get_called_class();
    	$this->mapperName = str_replace('Controller', 'Mapper', str_replace('\Controller\Admin\\', '\Mapper\\', $class));
        $this->entityName = str_replace('Controller', '', str_replace('\Controller\Admin\\', '\Entity\\', $class));
        $this->formName = str_replace('Controller', 'Form', str_replace('\Controller\Admin\\', '\Form\\', $class));

        $sub = explode('\\', str_replace('\Controller\\', '\\', $class));
        $this->moduleName = strtolower($sub[0]);

        $explode = explode('\\', $class);
        $this->controllerName = str_replace('-controller', '',trim(strtolower(preg_replace('@([A-Z])@', "-$1", $explode[3])), '-'));
    }

    /**
     * Get MapperInterface instance
     *
     * @return Form
     */
    public function getMapper()
    {
        if ($this->mapper === null) {
            $this->setMapper($this->getServiceLocator()->get($this->mapperName));
        }
        return $this->mapper;
    }

    /**
     * Set MapperInterface instance
     *
     * @param MapperInterface $mapper
     */
    public function setMapper(MapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Get Mapper name
     *
     * @return string
     */
    public function getMapperName()
    {
        return $this->mapperName;
    }

    /**
     * Set Mapper name
     *
     * @param string $mapperName
     */
    public function setMapperName($mapperName)
    {
        $this->mapperName = $mapperName;
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * Set entity name
     *
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    /**
     * Get module name
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set module name
     *
     * @param string $moduleName
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;
    }

    /**
     * Get controller name
     *
     * @return string
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * Set controller name
     *
     * @param string $controllerName
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;
    }

    /**
     * Get ViewModel
     *
     * @return string
     */
    public function getViewModel()
    {
        if ($this->viewModel === null) {
            $this->setViewModel(new ViewModel());
        }
        return $this->viewModel;
    }

    /**
     * Set ViewModel
     *
     * @param string $viewModel
     */
    public function setViewModel($viewModel)
    {
        $this->viewModel = $viewModel;
    }

    /**
     * Get form instance
     *
     * @return Form
     */
    public function getForm()
    {
        if (null === $this->form) {
            $this->setForm($this->getServiceLocator()->get($this->formName));
        }
        return $this->form;
    }

    /**
     * Set form instance
     *
     * @param Form $form
     */
    public function setForm(Form $form)
    {
        $this->form = $form;
    }
}