<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

 namespace CRUD\Controller\Admin;

 // CRUD
use CRUD\Controller\Controller;

// Pagination
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as PaginatorAdapter;
use Zend\Paginator\Paginator;


abstract class AbstractAdminController extends Controller
{
	/**
	 * It will list the elements in a table format with actions and toolbars
	 *
	 * @return ViewModel
	 */
	public function indexAction()
	{
		$helper = $this->getHelperList();
		$helper->moduleName = $this->getModuleName();
		$helper->controllerName = $this->getControllerName();

		// Order By
		$this->doOrder($helper);

		// Get data from database
		$data = $this->getMapper()->findAll($helper->orderBy, $helper->orderWay);

		// Create paginator
        $paginatorAdapter = $this->getMapper()->getPaginationAdapter();
        $paginator = new Paginator($paginatorAdapter);
        $page = 1;

        if ($this->params()->fromRoute('page')) {
        	$page = $this->params()->fromRoute('page');
        }

        $paginator->setCurrentPageNumber((int)$page)->setItemCountPerPage(10);

        // Create ViewModel instace
		$viewModel = $this->getViewModel();
		$viewModel->setTemplate('crud/layout/index');
		$viewModel->setVariables(
			array(
				'elements' 	  => $paginator->getCurrentItems(),
            	'paginator'   => $paginator,
            	'helper' 	  => $helper,
            	'search_form' => $this->doSearchForm(),
            	'list_form'   => $this->doListForm(),
			)
		);

		return $viewModel;
	}

	/**
	 * Display add form
	 *
	 * @return ViewModel
	 */
	public function addAction()
	{

		$form = $this->getForm();

		//********************

		$relatedTo = $this->params()->fromQuery('relatedTo');
		$relatedId = $this->params()->fromQuery('relatedId');
		$explode = explode('.', $relatedTo);

		//********************

		$request = $this->getRequest();
		if($request->isPost())
		{
			$entityName = $this->getEntityName();
			$element = new $entityName;

			$form->bind($element);
			$form->setData($request->getPost());

			if($form->isValid()) {
				
				$this->afterSaveObject($element);

				$this->getMapper()->save($element);

				//********************

				if($relatedTo && $relatedId) {
					return $this->redirect()->toRoute('admin/'.$explode[0].'/default', array('controller' => $explode[1], 'action' => 'view', 'id' => $relatedId));
				}

				//********************

				// Redirect to list of elements
				return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
			}
			else {
				/*
				echo '<PRE>'; 
                print_r($form->getMessages());      //Error messages
                print_r($form->getErrors());        //Error codes
                print_r($form->getErrorMessages()); //Any custom error messages
                die;
                */
			}
		}

		//********************

		$query = array();
		if($relatedTo && $relatedId){
			$query = array('relatedTo' => $relatedTo, 'relatedId' => $relatedId);
		}

		//********************

		// Set form attributes
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $this->url()->fromRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'add'), array('query' => $query)));
		$form->setAttribute('class', 'form-horizontal');

		$helper = $this->getHelperForm();
		$helper->moduleName = $this->getModuleName();
		$helper->controllerName = $this->getControllerName();

		// Create ViewModel instace
		$viewModel = $this->getViewModel();
		$viewModel->setTemplate('crud/layout/form');
		//$viewModel->setTerminal(true);
		$viewModel->setVariables(
			array(
				'form' 	 => $form,
            	'helper' => $helper,
			)
		);

		return $viewModel;
	}

	/**
	 * Display edit form
	 *
	 * @return ViewModel
	 */
	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'add'));
        }

        // Get the element with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $element = $this->getMapper()->find($id);
            if(!$element) {
                $this->flashMessenger()->setNamespace('error')->addMessage('Invalid element ID');
                return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
            }
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
        }


		$form = $this->getForm();
		$form->bind($element);

		$request = $this->getRequest();
		if($request->isPost()) {

			$form->setData($request->getPost());

			if($form->isValid()) {
				
				$this->getMapper()->save($element);

				// Redirect to list of elements
                return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
			} else {
				/*
				echo '<PRE>'; 
                print_r($form->getMessages());      //Error messages
                print_r($form->getErrors());        //Error codes
                print_r($form->getErrorMessages()); //Any custom error messages
                die;
                */
			}
		}

		// Set form attributes
		$form->setAttribute('method', 'post');
		$form->setAttribute('action', $this->url()->fromRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'edit', 'id' => $element->getId())));
		$form->setAttribute('class', 'form-horizontal');

		$helper = $this->getHelperForm();
		$helper->moduleName = $this->getModuleName();
		$helper->controllerName = $this->getControllerName();

		// Create ViewModel instace
		$viewModel = $this->getViewModel();
		$viewModel->setTemplate('crud/layout/form');
		$viewModel->setVariables(
			array(
				'form' 	  => $form,
				'element' => $element,
				'helper' => $helper,
			)
		);

		return $viewModel;
	}

	/**
	 * Display view page
	 *
	 * @return ViewModel
	 */
	public function viewAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'add'));
        }

        // Get the element with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $element = $this->getMapper()->find($id);
            if(!$element) {
                $this->flashMessenger()->setNamespace('error')->addMessage('Invalid element ID');
                return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
            }
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index'));
        }

        $helper = $this->getHelperView();
		$helper->moduleName = $this->getModuleName();
		$helper->controllerName = $this->getControllerName();

		/*if($orderList = $this->params()->fromQuery('orderList')) {
			if(isset($helper->lists[$orderList])) {
				$orderBy = $this->params()->fromQuery('orderBy');
				$orderWay = $this->params()->fromQuery('orderWay');
				if($orderBy && $orderWay) {
					$helper->lists[$orderList]->orderBy = $orderBy;
					$helper->lists[$orderList]->orderWay = $orderWay;
				}
			}
		}*/
		if ($this->params()->fromQuery('pageList')) {
        	$pageList = $this->params()->fromQuery('pageList');
        	if(isset($helper->lists[$pageList])) {
        		$page = $this->params()->fromQuery('page');
        		if($page) {
					$helper->lists[$pageList]->page = $page;
				}
        	}
        }


        
        // Create ViewModel instace
		$viewModel = $this->getViewModel();
		$viewModel->setTemplate('crud/layout/view');
		$viewModel->setVariables(
			array(
				'element' => $element,
            	'helper'  => $helper,
			)
		);

		return $viewModel;	
	}






	private function doSearchForm()
	{
		// Create form for multiple deliting, ordering and filtering
        $form = new \Zend\Form\Form;
        $form->setAttribute('action', $this->url()->fromRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index')));
        $form->setAttribute('method', 'post');
        $form->setAttribute('name', 'search_form');
        $form->setAttribute('id', 'search_form');

        return $form;
	}

	private function doListForm()
	{
		// Create form for multiple deliting, ordering and filtering
        $form = new \Zend\Form\Form;
        $form->setAttribute('action', $this->url()->fromRoute('admin/'.$this->getModuleName().'/default', array('controller' => $this->getControllerName(), 'action' => 'index')));
        $form->setAttribute('method', 'post');
        $form->setAttribute('name', 'list-form');
        $form->setAttribute('id', 'list-form');

        return $form;
	}

	private function doOrder($helper)
	{
		if($this->params()->fromRoute('order_by')) {
			$orderBy = $this->params()->fromRoute('order_by');
		} else {
			$orderBy = 'id';
		}
		$helper->orderBy = $orderBy;

		if($this->params()->fromRoute('order')) {
			$order = $this->params()->fromRoute('order');
		} else {
			$order = 'ASC';
		}
		$helper->orderWay = $order;
	}

	public function afterSaveObject($object) { return true; }

}