<?php
return array(
    'view_helpers' => array(
        'invokables' => array(
            'formckeditor' => 'CKEditor\Form\View\Helper\FormCKEditor'
        )
    )
);