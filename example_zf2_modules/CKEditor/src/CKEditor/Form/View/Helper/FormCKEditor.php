<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace CKEditor\Form\View\Helper;

// Zend
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\View\Helper\FormTextarea;


class FormCKEditor extends FormTextarea
{

	public function __invoke(ElementInterface $element = null) {

		$this->view->headScript()->prependFile($this->view->basePath() . '/js/tinymce/tinymce.min.js')
								 ->appendScript('tinymce.init({selector:"textarea.editor"});', 'text/javascript', array('id' => 'tinymce', 'noescape' => true));

		$class = $element->getAttribute('class');
		if($class != '')
			$class .= ' editor';
		else
			$class = 'editor';

		$element->setAttribute('class', $class);

		//return $this->render($element);
		return parent::render($element);
	}
}