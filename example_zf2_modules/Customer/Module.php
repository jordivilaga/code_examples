<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer;

// Zend
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

// Doctrine
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;


class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /*public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }*/

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                //...
            ),
            'factories' => array(

                // Customer Form
                'Customer\Form\CustomerForm' => function($sm) {
                    $form = new Form\Customer('customer', $sm->get('Doctrine\ORM\EntityManager'));
                    $form->setInputFilter(new Form\CustomerFilter());
                    $form->setHydrator(new DoctrineHydrator($sm->get('Doctrine\ORM\EntityManager'), 'Customer\Entity\Customer'));
                    return $form;
                },

                // Group Form
                'Customer\Form\GroupForm' => function($sm) {
                    $form = new Form\Group('customer', $sm->get('Doctrine\ORM\EntityManager'));
                    $form->setInputFilter(new Form\GroupFilter());
                    $form->setHydrator(new DoctrineHydrator($sm->get('Doctrine\ORM\EntityManager'), 'Customer\Entity\Group'));
                    return $form;
                },

                // Gender Form
                'Customer\Form\GenderForm' => function($sm) {
                    $form = new Form\Gender('gender', $sm->get('Doctrine\ORM\EntityManager'));
                    $form->setInputFilter(new Form\GenderFilter());
                    $form->setHydrator(new DoctrineHydrator($sm->get('Doctrine\ORM\EntityManager'), 'Customer\Entity\Gender'));
                    return $form;
                },

                //********************

                // Customer Mapper
                'Customer\Mapper\CustomerMapper' => function($sm) {
                    $mapper = new \CRUD\Mapper\DoctrineDbalMapper;
                    $mapper->setEntityManager($sm->get('Doctrine\ORM\EntityManager'));
                    $mapper->setEntityName('Customer\Entity\Customer');
                    return $mapper;
                },

                // Group Mapper
                'Customer\Mapper\GroupMapper' => function($sm) {
                    $mapper = new \CRUD\Mapper\DoctrineDbalMapper;
                    $mapper->setEntityManager($sm->get('Doctrine\ORM\EntityManager'));
                    $mapper->setEntityName('Customer\Entity\Group');
                    return $mapper;
                },

                // Gender Mapper
                'Customer\Mapper\GenderMapper' => function($sm) {
                    $mapper = new \CRUD\Mapper\DoctrineDbalMapper;
                    $mapper->setEntityManager($sm->get('Doctrine\ORM\EntityManager'));
                    $mapper->setEntityName('Customer\Entity\Gender');
                    return $mapper;
                },
            ),
        );
    }
}
