<?php 
namespace Customer;

return array(

    // Controllers in this module
    'controllers' => array(
        'invokables' => array(
            'Customer\Controller\Admin\Customer' => 'Customer\Controller\Admin\CustomerController',
            'Customer\Controller\Admin\Group'    => 'Customer\Controller\Admin\GroupController',
            'Customer\Controller\Admin\Gender'   => 'Customer\Controller\Admin\GenderController',
        ),
    ),

    // Routes for this module
    'router' => array(
        'routes' => array(
            'admin' => array(
                'child_routes' => array(
                    'customer' => array(
                        'type' => 'Literal',
                        'options' => array(
                            // Change this to something specific to your module
                            'route' => '/customer',
                            'defaults' => array(
                                // Change this value to reflect the namespace in which
                                // the controllers for your module are found
                                '__NAMESPACE__' => 'Customer\Controller\Admin',
                                'controller' => 'customer',
                                'action' => 'index',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            // This route is a sane default when developing a module;
                            // as you solidify the routes for your module, however,
                            // you may want to remove it and replace it with more
                            // specific routes.
                            'default' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/[:controller][/:action][/:id]',
                                    'constraints' => array(
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id'         => '[0-9]*',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),
                            'paginator' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/[:controller][/page/:page]',
                                    'constraints' => array(
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'page'       => '[0-9]*',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),
                            'sort' => array(
                                'type'    => 'Segment',
                                'options' => array(
                                    'route'    => '/[:controller][/order_by/:order_by][/:order]',
                                    'constraints' => array(
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'order_by' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'order' => 'ASC|DESC',
                                    ),
                                    'defaults' => array(
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),

    // Navigation
    'navigation' => array(
        'admin' => array(
            array(
                'label' => 'Customers',
                'route' => 'admin/customer/default',
                'pages' => array(
                    array(
                        'label' => 'Customers',
                        'route' => 'admin/customer/default',
                        'controller' => 'customer',
                        'action' => 'index',
                    ),
                    array(
                        'label' => 'Addresses',
                        'route' => 'admin/address/default',
                        'controller' => 'address',
                        'action' => 'index',
                    ),
                    array(
                        'label' => 'Groups',
                        'route' => 'admin/customer/default',
                        'controller' => 'group',
                        'action' => 'index',
                    ),
                    array(
                        'label' => 'Genders',
                        'route' => 'admin/customer/default',
                        'controller' => 'gender',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),

    // Doctrine configuration
    'doctrine' => array(

        // Standard configuration for the ORM from https://github.com/doctrine/DoctrineORMModule
        // ONLY THIS IS REQUIRED IF YOU USE Doctrine in the module
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
            )
        ),

        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    // Pick any listeners you need
                    'Gedmo\Timestampable\TimestampableListener',
                    'Gedmo\Tree\TreeListener',
                ),
            ),
        ),
    ),

    // View setup for this module
    'view_manager' => array(
        'template_path_stack' => array(
            'Customer' => __DIR__ . '/../view',
        ),
    ),
);
