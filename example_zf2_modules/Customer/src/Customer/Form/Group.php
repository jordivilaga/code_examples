<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Form;

// Doctrine
use Doctrine\ORM\EntityManager;

// Zend
use Zend\Form\Form;


class Group extends Form
{

    public function __construct($name = null, EntityManager $entityManager)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden',
            'attributes' => array(
                'type' => 'hidden'
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name (*)',
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Discount (%)',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save',
            ),
        ));

        $this->add(array(
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                ),
            ),
        ));
    }
}
