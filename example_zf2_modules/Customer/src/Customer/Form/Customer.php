<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Form;

// Doctrine
use Doctrine\ORM\EntityManager;

// Zend
use Zend\Form\Form;


class Customer extends Form
{

    public function __construct($name = null, EntityManager $entityManager)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden',
            'attributes' => array(
                'type' => 'hidden'
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name (*)',
            ),
        ));

        $this->add(array(
            'name' => 'firstname',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'First Name (*)',
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Last Name (*)',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'email',
            ),
            'options' => array(
                'label' => 'Email (*)',
            ),
        ));

        $this->add(array(
            'name' => 'birthday',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => 'Birthday',
            ),
        ));

        $this->add(array(
            'name' => 'newsletter', 
            'type'  => 'radio', 
            'options' => array(
                'label' => 'Nesletter',
                'value_options' => array(
                    '1' => 'Yes',
                    '0' => 'No',
                ),
            ),
            'attributes' => array(
                'value' => '1'
            )
        ));

        $this->add(array(
            'name' => 'optin', 
            'type'  => 'radio', 
            'options' => array(
                'label' => 'Opt In',
                'value_options' => array(
                    '1' => 'Yes',
                    '0' => 'No',
                ),
            ),
            'attributes' => array(
                'value' => '1'
            )
        ));

        //***********************************

        $this->add(array(
            'name' => 'group',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Group',
                'empty_option'    => '- Select a group -',
                'object_manager' => $entityManager,
                'target_class' => 'Customer\Entity\Group',
                'property' => 'name'
            )
        ));

        $this->add(array(
            'name' => 'gender',
            'type' => 'DoctrineModule\Form\Element\ObjectRadio',
            'options' => array(
                'label' => 'Gender',
                'object_manager' => $entityManager,
                'target_class' => 'Customer\Entity\Gender',
                'property' => 'name'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Save',
            ),
        ));

        $this->add(array(
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    //'timeout' => 600
                ),
            ),
        ));
    }
}
