<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Controller\Admin;

// CRUD
use CRUD\Controller\Admin\AbstractAdminController;
use CRUD\Helper\HelperList;
use CRUD\Helper\Form\HelperFormPlain;
use CRUD\Helper\HelperView;


class GenderController extends AbstractAdminController
{
	public function getHelperList()
	{
		$helper = new HelperList();
		$helper->title 	 = 'Genders';
		$helper->headers = array(
			'id' 	=> array('title' => 'ID', 'width' => '50'),
			'name' 	=> array('title' => 'Name', 
				'decorators' => array(
					'link' => array('module' => 'shipping', 'controller' => 'carrier', 'action' => 'view', 'id' => '{id}'),
				),
			),
		);

		return $helper;
	}

	public function getHelperForm()
	{
		$helper = new HelperFormPlain();
		$helper->title 	  = 'Gender';
		$helper->elements = array('type', 'name');

		return $helper;
	}

	public function getHelperView()
	{
		$helper = new HelperView();

		$helper->title 	  = 'Gender';

		$helper->elements = array(
			array(
				'name' => array('title' => 'Name'),
			),
		);

		return $helper;
	}
}
