<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Controller\Admin;

// CRUD
use CRUD\Controller\Admin\AbstractAdminController;
use CRUD\Helper\HelperList;
use CRUD\Helper\Form\HelperFormPlain;
use CRUD\Helper\HelperView;


class GroupController extends AbstractAdminController
{
	public function getHelperList()
	{
		$helper = new HelperList();
		$helper->title = 'Groups';
		$helper->headers = array(
			'id' 		=> array('title' => 'ID', 'width' => 50),
			'name' 		=> array('title' => 'Name'),
			'discount' 	=> array('title' => 'Discount', 'width' => 120, 'type' => 'percent'),
		);

		return $helper;
	}

	public function getHelperForm()
	{
		$helper = new HelperFormPlain();
		$helper->title 	  = 'Group';
		$helper->elements = array(
			'name',
			'discount',
		);

		return $helper;
	}

	public function getHelperView()
	{
		$helper = new HelperView();
		$helper->title = 'Group';
		$helper->elements = array(
			array(
				'name' => array('title' => 'Name'),
				'id' => array('title' => 'ID'),
			),
			array(
				'created' => array('title' => 'Created', 'type' => 'date', 'format' => 'Y-m-d H:i:s'),
				'updated' => array('title' => 'Updated', 'type' => 'date', 'format' => 'Y-m-d H:i:s'),
			),
			array(
				'discount' => array('title' => 'Dicount', 'type' => 'percent'),
			),
		);

		$customers = new HelperList();
		$customers->key = 'customers';
		$customers->title = 'Customers';
		$customers->route = array('module' => 'customer', 'controller' => 'customer');
		$customers->headers = array(
			'id' 		  => array('title' => 'ID', 'width' => 50),
			'gender!name' => array('title' => 'Gender', 'width' => 50),
			'lastname' 	  => array('title' => 'Last Name'),
			'firstname'	  => array('title' => 'First Name'),
			'email' 	  => array('title' => 'Email', 'width' => 220),
			'nesletter'   => array('title' => 'News.', 'type' => 'boolean', 'width' => 80, 'align' => 'center'),
			'optin'		  => array('title' => 'Opt In', 'type' => 'boolean', 'width' => 80, 'align' => 'center'),
			'created'	  => array('title' => 'Created', 'type' => 'date', 'format' => 'Y-m-d', 'width' => 120),
			'updated'	  => array('title' => 'Updated', 'type' => 'date', 'format' => 'Y-m-d', 'width' => 120),
			'active'	  => array('title' => 'Enabled', 'type' => 'boolean', 'width' => 60, 'align' => 'center'),
		);

		$helper->lists = array(
			'customers' => $customers
		);

		return $helper;
	}
}
