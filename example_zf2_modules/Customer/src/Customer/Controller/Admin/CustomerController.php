<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Customer\Controller\Admin;

// CRUD
use CRUD\Controller\Admin\AbstractAdminController;
use CRUD\Helper\HelperList;
use CRUD\Helper\Form\HelperFormPlain;
use CRUD\Helper\HelperView;


class CustomerController extends AbstractAdminController
{
	public function getHelperList()
	{
		$helper = new HelperList();
		$helper->title 	 = 'Customers';
		$helper->headers = array(
			'id' 		  => array('title' => 'ID', 'width' => 50),
			'gender!name' => array('title' => 'Gender', 'width' => 50),
			'lastname' 	  => array('title' => 'Last Name'),
			'firstname'	  => array('title' => 'First Name'),
			'email' 	  => array('title' => 'Email', 'type' => 'email', 'width' => 220),
			'nesletter'   => array('title' => 'News.', 'type' => 'boolean', 'width' => 80, 'align' => 'center'),
			'optin'		  => array('title' => 'Opt In', 'type' => 'boolean', 'width' => 80, 'align' => 'center'),
			'created'	  => array('title' => 'Created', 'type' => 'date', 'format' => 'Y-m-d', 'width' => 120),
			'updated'	  => array('title' => 'Updated', 'type' => 'date', 'format' => 'Y-m-d', 'width' => 120),
			'active'	  => array('title' => 'Enabled', 'type' => 'boolean', 'width' => 60, 'align' => 'center'),
		);

		return $helper;
	}

	public function getHelperForm()
	{
		$helper = new HelperFormPlain();
		$helper->title = 'Customer';
		$helper->elements = array(
			'gender',
			'firstname',
			'lastname',
			'email',
			//'birthday',
			'newsletter',
			'optin',
			'group',
		);

		return $helper;
	}

	public function getHelperView()
	{
		$helper = new HelperView();
		$helper->title = 'Customer';
		$helper->elements = array(
			array(
				'id' => array('title' => 'ID'),
				'active' => array('title' => 'Active', 'type' => 'boolean'),
			),
			array(
				'firstname' => array('title' => 'First Name'),
				'lastname' => array('title' => 'Last Name'),
			),
			array(
				'email' => array('title' => 'Email', 'type' => 'email'),
				'birthday' => array('title' => 'Birthday', 'type' => 'date', 'format' => 'Y-m-d'),
			),
			array(
				'newsletter' => array('title' => 'Newsletter', 'type' => 'boolean'),
				'optin' => array('title' => 'Opt In', 'type' => 'boolean'),
			),
			array(
				'created' => array('title' => 'Created', 'type' => 'date', 'format' => 'Y-m-d H:i:s'),
				'updated' => array('title' => 'Updated', 'type' => 'date', 'format' => 'Y-m-d H:i:s'),
			),
			array(
				'group!name' => array('title' => 'Group'),
			),
		);

		$addresses = new HelperList();
		$addresses->key = 'addresses';
		$addresses->title = 'Addresses';
		$addresses->route = array('module' => 'address', 'controller' => 'address');
		$addresses->toolbar = array(
			'btn' => array('title' => 'Create', 'module' => 'address', 'controller' => 'address', 'action' => 'add', 'query' => array()),
		);
		$addresses->headers = array(
			'id' 			=> array('title' => 'ID', 'width' => 50),
			'lastname' 		=> array('title' => 'Last Name'),
			'firstname' 	=> array('title' => 'First Name'),
			'address1' 		=> array('title' => 'Address', 'width' => 185),
			'postcode' 		=> array('title' => 'Zip/Postcode', 'width' => 65),
			'city' 			=> array('title' => 'City', 'width' => 185),
			'country!name' 	=> array('title' => 'Country', 'width' => 185),
		);

		$helper->lists = array(
			'addresses' => $addresses
		);

		return $helper;
	}
}
