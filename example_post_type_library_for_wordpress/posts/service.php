<?php

// Register Custom Post Type
$args = array(
		'menu_icon'           => get_stylesheet_directory_uri(). '/img/posts/services.png'
);
$service = new Custom_Post_Type( 'Service', $args );

// Custom columns
add_action("manage_posts_custom_column",  "service_custom_columns");
add_filter("manage_edit-service_columns", "service_edit_columns");

function service_edit_columns($columns){
	
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"service_image" => 'Image',
		"title" => "Service Title",
		"service_description" => "Description"
	);
	return $columns;
}

function service_custom_columns($column){

	switch ($column) {
		case "service_image":
			the_post_thumbnail(array(100,100));
			break;
		case "service_description":
			the_content();
			break;
	}
}




















