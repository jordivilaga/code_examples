<?php

// Register Custom Post Type
$args = array(
		'menu_icon'           => get_stylesheet_directory_uri(). '/img/posts/projects.png'
);
$project = new Custom_Post_Type( 'Project', $args);

// Register Custom Taxonomy
$labels = array(
		'name'                  => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
		'menu_name'             => __( 'Categories', 'text_domain' ),
		'all_items'             => __( 'All Categories', 'text_domain' ),
		'search_items'          => __( 'Search categories', 'text_domain' ),
		'add_or_remove_items'   => __( 'Add or remove categories', 'text_domain' ),
		'choose_from_most_used'	=> __( 'Choose from the most used categories', 'text_domain' )
);
$project->add_taxonomy( 'Category', array(), $labels);

// Register Custom Taxonomy
$project->add_taxonomy( 'Skill');


// Define the custom box
$project->add_meta_box(
	'Other Information',
	array(
		'Client Name' => 'text',
		'Award Winner' => 'checkbox'
	)
);

// Custom columns
add_action("manage_posts_custom_column",  "project_custom_columns");
add_filter("manage_edit-project_columns", "project_edit_columns");

function project_edit_columns($columns){
	
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"project_image" => 'Image',
		"title" => "Project Title",
		"project_description" => "Description",
		"project_categories" => "Categories",
		"project_skills" => "Skills"
	);
	return $columns;
}

function project_custom_columns($column){

    $post_id = get_the_ID();
	switch ($column) {
		case "project_image":
			the_post_thumbnail(array(100,100));
			break;
		case "project_description":
			the_content();
			break;
		case "project_categories":
			echo get_the_term_list($post_id, 'project_category', '', '</br>','');
			break;
		case "project_skills":
			echo get_the_term_list($post_id, 'project_skill', '', '</br>','');
			break;
	}
}

// Remove gallery from content
if(!is_admin()){
	function remove_gallery( $content) {
		$expr = '/\[gallery(.*?)\]/i';
		return preg_replace( $expr, '', $content);
	}
	add_filter( 'the_content', 'remove_gallery', 6);
}





















