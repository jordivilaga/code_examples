<?php

// Register Custom Post Type
$args = array(
	'menu_icon'           => get_stylesheet_directory_uri(). '/img/posts/application.png'
);
$application = new Custom_Post_Type( 'Application' , $args);

// Register Custom Taxonomy
$application->add_taxonomy( 'Feature' );

// Define the custom box
$application->add_meta_box(
	'Other Information',
	array(
		'Built with' => 'textarea',
		'Ideal for' => 'textarea',					
		'Videos' => 'textarea'
	)
);
