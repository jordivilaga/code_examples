<?php

// Register Custom Post Type
$args = array(
		'menu_icon'           => get_stylesheet_directory_uri(). '/img/posts/clients.png'
);
$client = new Custom_Post_Type( 'Client' , $args);

// Register Custom Taxonomy
$labels = array(
	'name'                  => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
	'menu_name'             => __( 'Categories', 'text_domain' ),
	'all_items'             => __( 'All Categories', 'text_domain' ),
	'search_items'          => __( 'Search categories', 'text_domain' ),
	'add_or_remove_items'   => __( 'Add or remove categories', 'text_domain' ),
	'choose_from_most_used'	=> __( 'Choose from the most used categories', 'text_domain' )
);
$client->add_taxonomy( 'Category', array(), $labels);

// Custom columns
add_action("manage_posts_custom_column",  "client_custom_columns");
add_filter("manage_edit-client_columns", "client_edit_columns");

function client_edit_columns($columns){

	$columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"client_image" => 'Image',
			"title" => "Client Title",
			"client_description" => "Description",
			"client_categories" => "Categories"
	);
	return $columns;
}

function client_custom_columns($column){

	switch ($column) {
		case "client_image":
			the_post_thumbnail(array(100,100));
			break;
		case "client_description":
			the_content();
			break;
		case "client_categories":
			echo get_the_term_list(get_the_ID(), 'client_category', '', '</br>','');
			break;
	}
}


