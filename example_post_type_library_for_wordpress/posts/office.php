<?php

// Register Custom Post Type
$args = array(
    'menu_icon' => get_stylesheet_directory_uri() . '/img/posts/offices.png'
);
$office = new Custom_Post_Type('Office', $args);

// Define the custom box
$office->add_meta_box(
    'Other Information', array(
        'Address 1' => 'text',
        'Address 2' => 'text',
        'Address 3' => 'text',
        'Postcode' => 'text',
        'City' => 'text',
        'State' => 'text',
        'Country' => 'text',
        'General Inquires Email' => 'text',
        'Telephone' => 'text',
    )
);

// Custom columns
add_action("manage_posts_custom_column", "office_custom_columns");
add_filter("manage_edit-office_columns", "office_edit_columns");

function office_edit_columns($columns) {

    $columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "title" => "Office Title",
        "office_address" => "Address",
        "office_city" => "City",
        "office_country" => "Country",
        "office_description" => "Description",
        "date" => "Date"
    );
    return $columns;
}

function office_custom_columns($column) {

    $post_id = get_the_ID();
    switch ($column) {
        case "office_description":
            the_content();
            break;
        case "office_address":
            echo get_post_meta($post_id, 'other_information_address_1', true);
            echo '<br/>';
            echo get_post_meta($post_id, 'other_information_address_2', true);
            break;
        case "office_city":
            echo get_post_meta($post_id, 'other_information_city', true);
            break;
        case "office_country":
            echo get_post_meta($post_id, 'other_information_country', true);
            break;
    }
}

