<?php

// Register Custom Post Type
$args = array(
    'menu_icon' => get_stylesheet_directory_uri() . '/img/posts/slides.png'
);
$slide = new Custom_Post_Type('Slide', $args, array(
    'menu_name' => __('Slideshow', 'text_domain'),
        ));

/*$slide->add_meta_box(
        'Other Information', array(
    'Horizontal Position' => 'select;Left|Center|Right',
    'Vertical Position' => 'select;Top|Middle|Bottom',
        )
);*/

// Custom columns
add_action("manage_posts_custom_column", "slide_custom_columns");
add_filter("manage_edit-slide_columns", "slide_edit_columns");

function slide_edit_columns($columns) {

    $columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "slide_image" => 'Image',
        "title" => "Project Title",
        "slide_description" => "Description",
    );
    return $columns;
}

function slide_custom_columns($column) {

    switch ($column) {
        case "slide_image":
            the_post_thumbnail('medium');
            break;
        case "slide_description":
            the_content();
            break;
    }
}

// Define additional "post thumbnails". Relies on MultiPostThumbnails to work
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(array(
        'label' => 'Background Image',
        'id' => 'feature-image-background',
        'post_type' => 'slide'
        )
    );
}