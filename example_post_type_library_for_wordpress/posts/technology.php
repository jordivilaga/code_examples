<?php

// Register Custom Post Type
$labels = array(
	'name'                => _x( 'Technologies', 'Post Type General Name', 'text_domain' ),
	'menu_name'           => __( 'Technologies', 'text_domain' ),
	'all_items'           => __( 'All Technologies', 'text_domain' ),
	'search_items'        => __( 'Search technologies', 'text_domain' ),
	'not_found'           => __( 'No technologies found', 'text_domain' ),
	'not_found_in_trash'  => __( 'No technologies found in Trash', 'text_domain' )
);
$technology = new Custom_Post_Type( 'Technology', array(), $labels);

// Define the custom box
$technology->add_meta_box(
	'Other Information',
	array(					
		'Video ID' => 'text'
	)
);


// Define additional "post thumbnails". Relies on MultiPostThumbnails to work
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(array(
        'label' => 'Video Image',
        'id' => 'feature-image-video',
        'post_type' => 'technology'
        )
    );
}