<?php

// Register Custom Post Type
$args = array(
	'menu_icon'           => get_stylesheet_directory_uri(). '/img/posts/jobs.png'
);
$job = new Custom_Post_Type( 'Job' , $args);

// Register Custom Taxonomy
$labels = array(
	'name'                  => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
	'menu_name'             => __( 'Categories', 'text_domain' ),
	'all_items'             => __( 'All Categories', 'text_domain' ),
	'search_items'          => __( 'Search categories', 'text_domain' ),
	'add_or_remove_items'   => __( 'Add or remove categories', 'text_domain' ),
	'choose_from_most_used'	=> __( 'Choose from the most used categories', 'text_domain' )
);
$job->add_taxonomy( 'Category', array(), $labels);

$job->add_taxonomy( 'Department' );

// Define the custom box
$job->add_meta_box(
    'Other Information',
    array(
        /*'Department' => 'text',*/
        /*'Salary' => 'text'*/
        'Location' => 'text'
    )
);