<?php

class Custom_Post_Type
{
	public $post_type_name;
	public $post_type_args;
	public $post_type_labels;

	/* Class constructor */
	public function __construct( $name, $args = array(), $labels = array() )
	{
		// Set some important variables
		$this->post_type_name		= strtolower( str_replace( ' ', '_', $name ) );
		$this->post_type_args 		= $args;
		$this->post_type_labels 	= $labels;

		// Add action to register the post type, if the post type doesnt exist
		if( !post_type_exists( $this->post_type_name ) )
		{
			add_action( 'init', array( &$this, 'register_post_type' ) );
		}

		// Listen for the save post hook
		$this->save();
	}

	/* Method which registers the post type */
	public function register_post_type()
	{
		//Capitilize the words and make it plural
		$name 		= ucwords( str_replace( '_', ' ', $this->post_type_name ) );
		$plural 	= $name . 's';

		// We set the default labels based on the post type name and plural. We overwrite them with the given labels.
		$labels = array_merge(

			// Default
			array(
				'name'                => _x( $plural, 'Post Type General Name', 'text_domain' ),
				'singular_name'       => _x( $name, 'Post Type Singular Name', 'text_domain' ),
				'menu_name'           => __( $plural, 'text_domain' ),
				'parent_item_colon'   => __( 'Parent '.$name.':', 'text_domain' ),
				'all_items'           => __( 'All '.$plural, 'text_domain' ),
				'view_item'           => __( 'View '.$name, 'text_domain' ),
				'add_new_item'        => __( 'Add New '.$name, 'text_domain' ),
				'add_new'             => __( 'New '.$name, 'text_domain' ),
				'edit_item'           => __( 'Edit '.$name, 'text_domain' ),
				'update_item'         => __( 'Update '.$name, 'text_domain' ),
				'search_items'        => __( 'Search '.strtolower($plural), 'text_domain' ),
				'not_found'           => __( 'No '.strtolower($plural).' found', 'text_domain' ),
				'not_found_in_trash'  => __( 'No '.strtolower($plural).' found in Trash', 'text_domain' )
			),
			
			// Given labels
			$this->post_type_labels

		);

		// Same principle as the labels. We set some default and overwite them with the given arguments.
		$args = array_merge(

			// Default
			array(
				'label'               => __( strtolower($name), 'text_domain' ),
				'description'         => __( $name.' information pages', 'text_domain' ),
				'labels'              => $labels,
				'supports'            => array( 'title' , 'editor' , 'thumbnail' ),
				//'taxonomies'          => array( 'category', 'post_tag' ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 5,
				//'menu_icon'           => '',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			),

			// Given args
			$this->post_type_args

		);

		// Register the post type
		register_post_type( $this->post_type_name, $args );
	}

	/* Method to attach the taxonomy to the post type */
	public function add_taxonomy( $name, $args = array(), $labels = array() )
	{
		if( ! empty( $name ) )
		{
			// We need to know the post type name, so the new taxonomy can be attached to it.
			$post_type_name = $this->post_type_name;

			// Taxonomy properties
			$taxonomy_name		= strtolower( $post_type_name .'_'. str_replace( ' ', '_', $name ) );
			$taxonomy_labels	= $labels;
			$taxonomy_args		= $args;

			if( !taxonomy_exists( $taxonomy_name ) )
			{
				//Capitilize the words and make it plural
				$name 		= ucwords( str_replace( '_', ' ', $name ) );
				$plural 	= $name . 's';

				// Default labels, overwrite them with the given labels.
				$labels = array_merge(

					// Default
					array(
						'name'                       => _x( $plural, 'Taxonomy General Name', 'text_domain' ),
						'singular_name'              => _x( $name, 'Taxonomy Singular Name', 'text_domain' ),
						'menu_name'                  => __( $plural, 'text_domain' ),
						'all_items'                  => __( 'All '.$plural, 'text_domain' ),
						'parent_item'                => __( 'Parent '.$name, 'text_domain' ),
						'parent_item_colon'          => __( 'Parent '.$name.':', 'text_domain' ),
						'new_item_name'              => __( 'New '.$name.' Name', 'text_domain' ),
						'add_new_item'               => __( 'Add New '.$name, 'text_domain' ),
						'edit_item'                  => __( 'Edit '.$name, 'text_domain' ),
						'update_item'                => __( 'Update '.$name, 'text_domain' ),
						'separate_items_with_commas' => __( 'Separate '.strtolower($name).' with commas', 'text_domain' ),
						'search_items'               => __( 'Search '.strtolower($plural), 'text_domain' ),
						'add_or_remove_items'        => __( 'Add or remove '.strtolower($plural), 'text_domain' ),
						'choose_from_most_used'      => __( 'Choose from the most used '.strtolower($plural), 'text_domain' ),
					),

					// Given labels
					$taxonomy_labels

				);

				// Default arguments, overwitten with the given arguments
				$args = array_merge(

					// Default
					array(
						'labels'                     => $labels,
						'hierarchical'               => true,
						'public'                     => true,
						'show_ui'                    => true,
						'show_admin_column'          => true,
						'show_in_nav_menus'          => true,
						'show_tagcloud'              => true,
					),

					// Given
					$taxonomy_args

				);

				// Add the taxonomy to the post type
				add_action( 'init',
					function() use( $taxonomy_name, $post_type_name, $args )
					{
						register_taxonomy( $taxonomy_name, $post_type_name, $args );
					}
				);
			}
			else
			{
				add_action( 'init',
					function() use( $taxonomy_name, $post_type_name )
					{
						register_taxonomy_for_object_type( $taxonomy_name, $post_type_name );
					}
				);
			}
		}
	}

	/* Attaches meta boxes to the post type */
	public function add_meta_box( $title, $fields = array(), $context = 'normal', $priority = 'default' )
	{
		if( ! empty( $title ) )
		{
			// We need to know the Post Type name again
			$post_type_name = $this->post_type_name;

			// Meta variables
			$box_id 		= strtolower( str_replace( ' ', '_', $title ) );
			$box_title		= ucwords( str_replace( '_', ' ', $title ) );
			$box_context	= $context;
			$box_priority	= $priority;

			// Make the fields global
			global $custom_fields;
			$custom_fields[$post_type_name][$title] = $fields;

			add_action( 'admin_init',
				function() use( $box_id, $box_title, $post_type_name, $box_context, $box_priority, $fields )
				{
					add_meta_box(
					$box_id,
					$box_title,
					function( $post, $data )
					{
						global $post;
	
						// Nonce field for some validation
						wp_nonce_field( plugin_basename( __FILE__ ), 'custom_post_type' );
	
						// Get all inputs from $data
						$custom_fields = $data['args'][0];
	
						// Get the saved values
						$meta = get_post_custom( $post->ID );

						// Check the array and loop through it
						if( ! empty( $custom_fields ) )
						{
							
							echo '<table>';
							
							/* Loop through $custom_fields */
							foreach( $custom_fields as $label => $type )
							{
								$field_id_name 	= strtolower( str_replace( ' ', '_', $data['id'] ) ) . '_' . strtolower( str_replace( ' ', '_', $label ) );
								
								$type_arr = explode( ';' , $type);
								
								echo '<tr>';
								
								switch ($type_arr[0]) {
									
									case 'text':
										
										echo '<td style="text-align:right;">';
										echo '<label for="' . $field_id_name . '">' . $label . ':&nbsp;</label>';
										echo '</td>';
										echo '<td>';
										echo '<input type="text" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $meta[$field_id_name][0] . '" />';
										echo '</td>';
										break;
										
									case 'textarea':
									
										echo '<td style="text-align:right;">';
										echo '<label for="' . $field_id_name . '" style="vertical-align:top;">' . $label . ':&nbsp;</label>';
										echo '</td>';
										echo '<td>';
										echo '<textarea cols="60" rows="6" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" >'. $meta[$field_id_name][0].'</textarea>';
										echo '</td>';
										break;
										
									case 'select':
										
										echo '<td style="text-align:right;">';
										$values = explode( '|' , $type_arr[1]);
										echo '<label for="' . $field_id_name . '">' . $label . ':&nbsp;</label>';
										echo '</td>';
										echo '<td>';
										echo '<select name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $meta[$field_id_name][0] . '">';
										foreach($values as $value) {
											$selected = ( strtolower($meta[$field_id_name][0]) == strtolower($value) ) ? 'selected="selected"' : '';
											echo '<option value="'.strtolower(str_replace( ' ', '-', $value )).'" '.$selected.'>'.$value.'</option>';
										}
										echo '</select>';
										echo '</td>';
										break;
										
									case 'checkbox':

										echo '<td></td>';
										echo '<td>';
										$checked = ( $meta[$field_id_name][0] == 1 )? 'checked="checked"' : '';
										echo '<input type="checkbox" name="custom_meta[' . $field_id_name . ']" id="' . $field_id_name . '" value="' . $meta[$field_id_name][0] . '" '.$checked.' />&nbsp;';
										echo '<label for="' . $field_id_name . '">' . $label . '</label>';
										echo '</td>';
										break;
								}
								
								echo '</tr>';
							}
							
							echo '</table>';
						}
	
					},
					$post_type_name,
					$box_context,
					$box_priority,
					array( $fields )
					);
				}
			);
		}

	}

	/* Listens for when the post type being saved */
	public function save()
	{
		// Need the post type name again
		$post_type_name = $this->post_type_name;

		add_action( 'save_post',
			function() use( $post_type_name )
			{
				// Deny the wordpress autosave function
				if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
	
				if ( ! wp_verify_nonce( $_POST['custom_post_type'], plugin_basename(__FILE__) ) ) return;
	
				global $post;
	
				if( isset( $_POST ) && isset( $post->ID ) && get_post_type( $post->ID ) == $post_type_name )
				{
					global $custom_fields;
					
					// Loop through each meta box
					foreach( $custom_fields[$post_type_name] as $title => $fields )
					{
						// Loop through all fields
						foreach( $fields as $label => $type )
						{
							$field_id_name 	= strtolower( str_replace( ' ', '_', $title ) ) . '_' . strtolower( str_replace( ' ', '_', $label ) );
							
							$type_arr = explode( ';' , $type);
							
							switch ($type_arr[0]) {
							
								case 'checkbox':
									
									if(isset($_POST['custom_meta'][$field_id_name]))
										$_POST['custom_meta'][$field_id_name] = 1;
									else
										$_POST['custom_meta'][$field_id_name] = 0;
										
									break;
							}
							
							update_post_meta( $post->ID, $field_id_name, $_POST['custom_meta'][$field_id_name]);
						}
	
					}
				}
			}
		);
	}
}



?>