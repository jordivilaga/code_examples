<?php

class PostModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getPosts()
	{
		$posts = $this->_db->query("SELECT * FROM post");
		return $posts->fetchall();
	}
}