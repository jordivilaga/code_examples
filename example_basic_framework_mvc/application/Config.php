<?php

define('BASE_URL', 'http://localhost/proyecto_mvc/');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');

define('APP_NAME', 'MVC Framework');
define('APP_SLOGAN', 'My first MVC framework');
define('APP_COMPANY', 'www.jordivila.es');

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_NAME', 'proyecto_mvc');
define('DB_CHARSET', 'utf8');