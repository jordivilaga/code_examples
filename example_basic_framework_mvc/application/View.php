<?php

class View
{
	private $_controller;

	public function __construct(Request $request)
	{
		$this->_controller = $request->getController();
	}

	public function render($view, $item = false)
	{
		$pathView = ROOT . 'views' . DS . $this->_controller . DS . $view . '.phtml';

		if( is_readable($pathView) ) {
			include_once ROOT . 'views' . DS . 'layout' . DS . DEFAULT_LAYOUT . DS . 'header.php';
			include_once $pathView;
			include_once ROOT . 'views' . DS . 'layout' . DS . DEFAULT_LAYOUT . DS . 'footer.php'; 
		}
		else {
			throw new Exception("View not found", 1);
		}
	}
}