<?php
if (!defined('_PS_VERSION_'))
	exit;

include(__DIR__ . "/libraries/DJJob.php");
include(__DIR__ . "/libraries/Job.php");

class Dropshipping extends Module
{
	const TYPE_INT = 1;

	public $product;
	public $product_image;
	public $count = 0;

	public $map_products;
	public $categoriesByDropshipping = array();
	public $categoriesByNameAndParent = array();

	public $category_id = false;

	public $modified = false;

	public $languages = false;

	public function __construct()
	{
		$this->name = 'dropshipping';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Jordi Vila Gallardo';

		parent::__construct();

		$this->displayName = $this->l('Drop Shipping Module');
		$this->description = $this->l('Drop Shipping Module');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

		Category::$definition['fields']['id_drop_shipping'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId');

		$this->getProducts();
		$this->getCategories();
 
		DJJob::configure("mysql:host=localhost;dbname=yanelex_intimate", array ( 'mysql_user' => 'root', 'mysql_pass' => 'root'));
		

		//$worker = new DJWorker(array ( "count" => 1, "sleep" => 0 ));
		//$worker->start();

		//die('done');
		$this->context->smarty->assign('module_name', $this->name);
	}

	public function install()
	{
		if (!parent::install() ||
			!$this->_createDatabase())
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall() ||
			!$this->_deleteDatabase())
			return false;
		return true;
	}

	private function _createDatabase()
	{
		$res = (bool) Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'category` ADD `id_drop_shipping` INT NULL DEFAULT NULL');
		$res &= (bool) Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'product` ADD `id_drop_shipping` INT NULL DEFAULT NULL');
		return $res;
	}

	private function _deleteDatabase()
	{
		$res = (bool) Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'category` DROP `id_drop_shipping`');
		$res &= (bool) Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'product` DROP `id_drop_shipping`');
		return $res;
	}

	public function getContent()
	{
		$message = '';

		if (Tools::isSubmit('submit_'.$this->name)) {
			$message = $this->_updateDropShipping();
			//$message = $this->_saveContent();
		}

		$this->_displayContent($message);

		return $this->display(__FILE__, 'settings.tpl');
	}

	private function _displayContent($message)
	{
		$this->context->smarty->assign(array(
			'message' => $message,
			'MOD_DROPSHIPPING_LAST_UPDATED' => Configuration::get('MOD_DROPSHIPPING_LAST_UPDATE'),
		));
	}

	private function _saveContent()
	{
		$message = '';

		if (Configuration::updateValue('MOD_SKELETON_NAME', Tools::getValue('MOD_SKELETON_NAME')) &&
			Configuration::updateValue('MOD_SKELETON_COLOR', Tools::getValue('MOD_SKELETON_COLOR')))
			$message = $this->displayConfirmation($this->l('Your settings have been saved'));
		else
			$message = $this->displayError($this->l('There was an error while saving your settings'));

		return $message;
	}

	private function _updateDropShipping()
	{
		$message = '';

		$message = $this->displayConfirmation($this->l('The products and categories have been updated'));
		//$message = $this->displayError($this->l('There was an error while saving your settings'));

		// Create the parser
		$parser = xml_parser_create();

		xml_set_element_handler($parser, array($this,'startElemHandler'), array($this,'endElemHandler'));
		xml_set_character_data_handler($parser, array($this,'characters'));
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);

		// Read the contents of the links file
		//$file = implode("", file('http://www.dropshipping.co.uk/catalog/xml3.xml'));
		$file = implode("", file('http://www.dropshipping.co.uk/catalog/xml_id_xl_multi_info.xml'));

		// Output each link 
		xml_parse($parser, $file);

		// Clean up - we're done
		xml_parser_free($parser);
		return $message;
	}

	function startElemHandler($parser, $name, $attribs)
	{
		global $elementName;
		$elementName = $name;

		if (strcasecmp($name, "STOREITEMS") == 0) {

	  	}
	  	if (strcasecmp($name, "CATEGORY") == 0) {

			//echo '<PRE>'; print_r($attribs["name"]);
			$categories = explode(">", $attribs["name"]);
			$home_parent_id = (int) Configuration::get('PS_HOME_CATEGORY');

			$id_drop_shipping = (int) $attribs["id"];

			if(isset($this->categoriesByDropshipping[$id_drop_shipping]))
			{
				$this->category_id = $this->categoriesByDropshipping[$id_drop_shipping];
			}
			else
			{
				$current_parent_id = 0;

				foreach ($categories as $key => $categoryname)
				{
					$name = trim(ucwords(strtolower($categoryname)));
					$link_rewrite = Tools::link_rewrite($name);

					if($current_parent_id == 0)
					{
						$parent_id = $home_parent_id;
					}
					else
					{
						$parent_id = $current_parent_id;
					}

					if(!isset($this->categoriesByNameAndParent[$link_rewrite][$parent_id]))
					{
						if(!empty($name) OR $name !='') {
							//echo '<PRE>'; print_r($name);
							$link_rewrite = Tools::link_rewrite($name);
							$id_lang = Context::getContext()->language->id;

							$category = new Category();
							$category->name = $this->createMultiLangField($name);
							$category->link_rewrite = $this->createMultiLangField($link_rewrite);
							$specific_price->id_shop = $this->context->shop->id;
							$category->active = 1;
							$category->id_parent = $parent_id;

							if((count($categories) - 1) == $key)
							{
								$category->id_drop_shipping = $attribs["id"];
							}

							$category->save();

							if((count($categories) - 1) == $key)
							{
								$this->categoriesByDropshipping[$category_id->id_drop_shipping] = $category->id;
							}

						}

						$this->categoriesByNameAndParent[$link_rewrite][$parent_id] = $category->id;
						$current_parent_id = $category->id;
					}
					else
					{
						$current_parent_id = $this->categoriesByNameAndParent[$link_rewrite][$parent_id];
					}
				}

				$this->category_id = $current_parent_id;
			}

	  	}
	  	if (strcasecmp($name, "PRODUCT") == 0) {
	  		// Add new field definition to save the id_drop_shipping
	  		Product::$definition['fields']['id_drop_shipping'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId');
	  		
	  		$id_drop_shipping = (int) $attribs["ITEM"];
	  		if(isset($this->map_products[$id_drop_shipping]) AND $this->map_products[$id_drop_shipping] > 0) {
	  			$this->product = new Product($this->map_products[$id_drop_shipping]);
	  		} else {
	  			$this->product = new Product();
	  		}

	  		$this->product->id_drop_shipping = (int) $attribs["ITEM"];
	  	}
	}

	function endElemHandler($parser, $name)
	{
	  	if (strcasecmp($name, "STOREITEMS") == 0) {

	  	}
	  	if (strcasecmp($name, "CATEGORY") == 0) {

	  	}
	  	if (strcasecmp($name, "PRODUCT") == 0) {
	  		$this->product->id_category_default = (int) $this->category_id;
	  		if($this->count < 5){
	  			DJJob::enqueue(new Job($this->product));

	  			$worker = new DJWorker(array ( "count" => 1, "sleep" => 0 ));
				$worker->start();

				die('done');
	  			//$this->product->save();
	  			/*$this->product->addToCategories(array((int) $this->category_id));
	  			$this->count = $this->count + 1;

	  			$url = trim('http://www.dropshipping.co.uk/catalog/images/'.$this->product_image);
	  			$url = str_replace(' ', '%20', $url);

  				$image = new Image();
				$image->id_product = (int)$this->product->id;
				$image->position = 1;
				$image->cover = true;

				// file_exists doesn't work with HTTP protocol
				if (@fopen($url, 'r') == false)
					$error = true;
				else if (($field_error = $image->validateFields(UNFRIENDLY_ERROR, true)) === true &&
					($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $image->add())
				{
					// associate image to selected shops
					$image->associateTo($shops);
					if (!$this->copyImg($product->id, $image->id, $url))
					{
						$image->delete();
						$this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
					}
				}
				else
					$error = true;*/
	  			
	  		}
	  	}
	}

	function characters($parser, $data)
	{
		global $elementName;

		switch ($elementName) {
			case 'NAME':
				$this->product->name = $this->createMultiLangField($data);
				$link_rewrite = Tools::link_rewrite($data);
				$this->product->link_rewrite = $this->createMultiLangField($link_rewrite);
				break;
			
			case 'WEIGHT':
				$this->product->weight = $data;
				break;

			case 'MODEL':
				$this->product->reference = $data;
				break;

			case 'PRICE':
				$this->product->wholesale_price = (float)$data;
				break;

			case 'RRP':
				$this->product->price = $data;
				// If a tax is already included in price, withdraw it from price
				$this->product->price = (float)number_format($this->product->price / (1 + 20 / 100), 6, '.', '');
				break;

			case 'IMAGE':
				$this->product_image = $data;
				break;

			case 'DESCRIPTION':
				break;

			default:
				# code...
				break;
		}
	}

	function createMultiLangField($field)
	{
		if(!$this->languages)
		{
			$this->languages = Language::getLanguages(false);
		}
		$res = array();
		foreach ($this->languages as $lang) 
		{
			$res[$lang['id_lang']] = $field;
		}
		return $res;
	}

	/**
	 * copyImg copy an image located in $url and save it in a path
	 * according to $entity->$id_entity .
	 * $id_image is used if we need to add a watermark
	 *
	 * @param int $id_entity id of product or category (set in entity)
	 * @param int $id_image (default null) id of the image if watermark enabled.
	 * @param string $url path or url to use
	 * @param string entity 'products' or 'categories'
	 * @return void
	 */
	protected static function copyImg($id_entity, $id_image = null, $url, $entity = 'products')
	{
		$tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
		$watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

		switch ($entity)
		{
			default:
			case 'products':
				$image_obj = new Image($id_image);
				$path = $image_obj->getPathForCreation();
			break;
			case 'categories':
				$path = _PS_CAT_IMG_DIR_.(int)$id_entity;
			break;
		}
		$url = str_replace(' ', '%20', trim($url));

		// Evaluate the memory required to resize the image: if it's too much, you can't resize it.
		if (!ImageManager::checkImageMemoryLimit($url))
			return false;

		// 'file_exists' doesn't work on distant file, and getimagesize make the import slower.
		// Just hide the warning, the traitment will be the same.
		if (@copy($url, $tmpfile))
		{
			ImageManager::resize($tmpfile, $path.'.jpg');
			$images_types = ImageType::getImagesTypes($entity);
			foreach ($images_types as $image_type)
				ImageManager::resize($tmpfile, $path.'-'.stripslashes($image_type['name']).'.jpg', $image_type['width'], $image_type['height']);

			if (in_array($image_type['id_image_type'], $watermark_types))
				Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
		}
		else
		{
			unlink($tmpfile);
			return false;
		}
		unlink($tmpfile);
		return true;
	}

	public function getProducts()
	{
		if(!$this->map_products)
		{
			$id_lang = Context::getContext()->language->id;

			$this->map_products = array();
			$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT p.`id_product`, p.`id_drop_shipping`, pl.`name` FROM `'._DB_PREFIX_.'product` p
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`'.Shop::addSqlRestrictionOnLang('pl').')
				'.Shop::addSqlAssociation('product', 'p').'
				WHERE pl.`id_lang` = '.(int)$id_lang
			);

			foreach ($row as $val)
				$this->map_products[$val['id_drop_shipping']] = $val['id_product'];

			return $this->map_products;
		}

		return $this->map_products;
	}

	public function getCategories()
	{
		if(!$this->categories)
		{
			$id_lang = Context::getContext()->language->id;

			$categories = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT c.`id_category`, c.`id_drop_shipping`, c.`id_parent`, c.`level_depth`, cl.`name`, cl.`link_rewrite` FROM `'._DB_PREFIX_.'category` c
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
				'.Shop::addSqlAssociation('category', 'c').'
				WHERE cl.`id_lang` = '.(int)$id_lang

			);

			foreach ($categories as $key => $category) {
				if($category['id_drop_shipping'])
				{
					$this->categoriesByDropshipping[$category['id_drop_shipping']] = $category['id_category'];
				}
				$this->categoriesByNameAndParent[$category['link_rewrite']][$category['id_parent']] = $category['id_category'];
			}

			return $this->categories;
		}

		return $this->categories;
	}
}

?>
