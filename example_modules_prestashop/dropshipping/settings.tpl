{$message}

<fieldset>
	<legend>Settings</legend>
	<form method="post">
		<p>
			<label for="MOD_DROPSHIPPING_LAST_UPDATED">Last Update:</label>
			<input id="MOD_DROPSHIPPING_LAST_UPDATED" name="MOD_DROPSHIPPING_LAST_UPDATED" type="text" value="{$MOD_DROPSHIPPING_LAST_UPDATED}" size="25" readonly/>
		</p>
		<p>
			<label>&nbsp;</label>
			<input id="submit_{$module_name}" name="submit_{$module_name}" type="submit" value="Update" class="button" />
		</p>
	</form>
</fieldset>
