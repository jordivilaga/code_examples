<?php

if (!defined('_PS_VERSION_'))
    exit;

class Sagepay extends PaymentModule {

    private $_html = '';
    private $_postErrors = array();
    public $extra_mail_vars;

    public function __construct() {
        
        $this->name = 'sagepay';
        $this->tab = 'payments_gateways';
        $this->version = '1.5';
        $this->author = 'Jordi Vila';

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        parent::__construct();
        
        /** Backward compatibility */
        //require(_PS_MODULE_DIR_ . '/sagepay/backward_compatibility/backward.php');

        $this->displayName = $this->l('Sagepay');
        $this->description = $this->l('Sagepay payment module.');
    }

    public function install() {
        
        if (!parent::install() || 
                !$this->registerHook('payment') || 
                !$this->registerHook('paymentReturn'))
            return false;
        return true;
    }

    public function uninstall() {
        
        if (!Configuration::deleteByName('SAGEPAY_ID')
                || !Configuration::deleteByName('SAGEPAY_CRYPTKEY')
                || !Configuration::deleteByName('SAGEPAY_DEMO_MODE')
                || !Configuration::deleteByName('SAGEPAY_3DSECURE')
                || !Configuration::deleteByName('SAGEPAY_DEFAULT_CURRENCY')
                || !parent::uninstall())
            return false;
        return true;
    }

    private function _postValidation() {
        
        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('sagepay_id'))
                $this->_postErrors[] = $this->l('Sagepay ID is required.');
            elseif (!Tools::getValue('sagepay_cryptkey'))
                $this->_postErrors[] = $this->l('Sagepay Crypt Key is required.');
        }
    }

    private function _postProcess() {
        
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('SAGEPAY_ID', Tools::getValue('sagepay_id'));
            Configuration::updateValue('SAGEPAY_CRYPTKEY', Tools::getValue('sagepay_cryptkey'));
            Configuration::updateValue('SAGEPAY_DEMO_MODE', Tools::getValue('sagepay_demo_mode'));
            Configuration::updateValue('SAGEPAY_3DSECURE', Tools::getValue('sagepay_3dsecure'));
            Configuration::updateValue('SAGEPAY_DEFAULT_CURRENCY', Tools::getValue('sagepay_default_currency'));
        }
        $this->_html .= '<div class="conf confirm"> ' . $this->l('Settings updated') . '</div>';
    }

    private function _displayForm() {
        $this->_html .=
            '<form action="' . Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']) . '" method="post">
                <fieldset>
                    <legend><img src="../img/admin/contact.gif" />' . $this->l('Sagepay configuration') . '</legend>
                    <table border="0" width="500" cellpadding="0" cellspacing="0" id="form">
                        <tr>
                            <td colspan="2">' . $this->l('Please specify the bank wire account details for customers') . '</td>
                        </tr>
                        <tr>
                            <td width="130" style="height: 35px;">' . $this->l('Username') . '</td>
                            <td><input type="text" name="sagepay_id" value="' . htmlentities(Tools::getValue('sagepay_id', Configuration::get('SAGEPAY_ID')), ENT_COMPAT, 'UTF-8') . '" /></td>
                        </tr>
                        <tr>
                            <td width="130" style="vertical-align: top;">' . $this->l('Crypt Key') . '</td>
                            <td><input type="text" name="sagepay_cryptkey" value="' . htmlentities(Tools::getValue('sagepay_cryptkey', Configuration::get('SAGEPAY_CRYPTKEY')), ENT_COMPAT, 'UTF-8') . '" /></td>
                        </tr>
                        <tr>
                            <td width="130" style="vertical-align: top;">' . $this->l('Use default currency?') . '</td>
                            <td>
                                <input type="radio" name="sagepay_default_currency" value="0" style="vertical-align: middle;" ' . (!Tools::getValue('sagepay_default_currency', Configuration::get('SAGEPAY_DEFAULT_CURRENCY')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #900;">' . $this->l('No') . '</span>&nbsp;
                                <input type="radio" name="sagepay_default_currency" value="1" style="vertical-align: middle;" ' . (Tools::getValue('sagepay_default_currency', Configuration::get('SAGEPAY_DEFAULT_CURRENCY')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #080;">' . $this->l('Yes') . '</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="130" style="vertical-align: top;">' . $this->l('3D Secure?') . '</td>
                            <td>
                                <input type="radio" name="sagepay_3dsecure" value="0" style="vertical-align: middle;" ' . (!Tools::getValue('sagepay_3dsecure', Configuration::get('SAGEPAY_3DSECURE')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #900;">' . $this->l('Disable') . '</span>&nbsp;
                                <input type="radio" name="sagepay_3dsecure" value="1" style="vertical-align: middle;" ' . (Tools::getValue('sagepay_3dsecure', Configuration::get('SAGEPAY_3DSECURE')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #080;">' . $this->l('Activate') . '</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="130" style="vertical-align: top;">' . $this->l('Mode') . '</td>
                            <td>
                                <input type="radio" name="sagepay_demo_mode" value="0" style="vertical-align: middle;" ' . (!Tools::getValue('sagepay_demo_mode', Configuration::get('SAGEPAY_DEMO_MODE')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #900;">' . $this->l('Test') . '</span>&nbsp;
                                <input type="radio" name="sagepay_demo_mode" value="1" style="vertical-align: middle;" ' . (Tools::getValue('sagepay_demo_mode', Configuration::get('SAGEPAY_DEMO_MODE')) ? 'checked="checked"' : '') . ' />
                                <span style="color: #080;">' . $this->l('Production') . '</span>
                            </td>
                        </tr>
                        <tr><td colspan="2" align="center"><input class="button" name="btnSubmit" value="' . $this->l('Update settings') . '" type="submit" /></td></tr>
                    </table>
                </fieldset>
            </form>';
    }

    public function getContent() {
        $this->_html = '<h2>' . $this->displayName . '</h2>';

        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors))
                $this->_postProcess();
            else
                foreach ($this->_postErrors as $err)
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
        }
        else
            $this->_html .= '<br />';

        $this->_displayForm();

        return $this->_html;
    }

    public function hookPayment($params) {
        
        if (!$this->active)
            return;
        
        if (!$this->checkCurrency($params['cart']))
            return;
        
        $this->smarty->assign(array(
            'this_path' => $this->_path,
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
        ));
        
        return $this->display(__FILE__, 'payment.tpl');
    }

    public function hookPaymentReturn($params) {

        if (!$this->active)
            return;
        
        $state = $params['objOrder']->getCurrentState();
        
        if ($state == Configuration::get('PS_OS_PAYMENT')) {
            $this->smarty->assign(array(
                'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'status' => 'ok',
                'id_order' => $params['objOrder']->id
            ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference)) {
                $this->smarty->assign('reference', $params['objOrder']->reference);
            }
        } else {
            $this->smarty->assign('status', 'failed');
        }
        
        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function checkCurrency($cart) {
        
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module))
            foreach ($currencies_module as $currency_module)
                if ($currency_order->id == $currency_module['id_currency'])
                    return true;
        return false;
    }
 
    public static function getShopDomainSsl($http = false, $entities = false) {
        
        if (method_exists('Tools', 'getShopDomainSsl')) {
            return Tools::getShopDomainSsl($http, $entities);
        } else {
            if (!($domain = Configuration::get('PS_SHOP_DOMAIN_SSL')))
                $domain = self::getHttpHost();
            if ($entities)
                $domain = htmlspecialchars($domain, ENT_COMPAT, 'UTF-8');
            if ($http)
                $domain = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://') . $domain;
            return $domain;
        }
    }
    
    public function to_array($input) {

        $values = array("Status", "StatusDetail", "VendorTxCode", "VPSTxId", "TxAuthNo", "Amount", "AVSCV2", "AddressResult", "PostCodeResult", "CV2Result", "GiftAid", "3DSecureStatus", "CAVV", "AddressStatus", "CardType", "Last4Digits", "PayerStatus", "CardType");

        $output = array();
        $resultArray = array();

        for ($i = count($values) - 1; $i >= 0; $i--) {
            $start = strpos($input, $values[$i]);
            if ($start !== false) {
                $resultArray[$i]->start = $start;
                $resultArray[$i]->token = $values[$i];
            }
        }

        sort($resultArray);
        for ($i = 0; $i < count($resultArray); $i++) {
            $valueStart = $resultArray[$i]->start + strlen($resultArray[$i]->token) + 1;
            if ($i == (count($resultArray) - 1)) {
                $output[$resultArray[$i]->token] = substr($input, $valueStart);
            } else {
                $valueLength = $resultArray[$i + 1]->start - $resultArray[$i]->start - strlen($resultArray[$i]->token) - 2;
                $output[$resultArray[$i]->token] = substr($input, $valueStart, $valueLength);
            }
        }
        return $output;
    }

    public function xor_crypt($input, $key) {
        $keyList = array();
        $output = "";
        for ($i = 0; $i < strlen($key); $i++) {
            $keyList[$i] = ord(substr($key, $i, 1));
        }
        for ($i = 0; $i < strlen($input); $i++) {
            if (strlen($key))
                $output .= chr(ord(substr($input, $i, 1)) ^ ($keyList[$i % strlen($key)]));
            else
                $output .= chr(ord(substr($input, $i, 1)) ^ 0);
        }
        return $output;
    }

}
