<?php

/**
 * @since 1.5.0
 */
class SagepayValidationModuleFrontController extends ModuleFrontController {

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess() {

        $cart = $this->context->cart;
        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module)
            if ($module['name'] == 'sagepay') {
                $authorized = true;
                break;
            }
        if (!$authorized)
            die($this->module->l('This payment method is not available.', 'validation'));

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $currency = $this->context->currency;
        $total = (float) $cart->getOrderTotal(true, Cart::BOTH);

        //Check for crypt GET
        $strCrypt = Tools::getValue('crypt');
        
        if (strlen($strCrypt)==0) {
            
        }
        
        $sage = new Sagepay();

        //Get and uncrypt validation message
        $strCrypt = str_replace(" ","+",$strCrypt);
        $unBased = base64_decode($strCrypt);
        $strDecoded = $sage->xor_crypt($unBased,Configuration::get('SAGEPAY_CRYPTKEY'));
        $values = $sage->to_array($strDecoded);

        $Status=$values['Status'];
        
        $mailVars = array();
        
        if ($Status == "OK" OR $Status == "AUTHENTICATED" OR $Status == "REGISTERED")
            $this->module->validateOrder($cart->id, _PS_OS_PAYMENT_, $total, $this->module->displayName, NULL, $mailVars, (int) $currency->id, false, $customer->secure_key);
        else
            $this->module->validateOrder($cart->id, _PS_OS_ERROR_, $total, $this->module->displayName, NULL, $mailVars, (int) $currency->id, false, $customer->secure_key);
        
        Tools::redirect('index.php?controller=order-confirmation&id_cart=' . $cart->id . '&id_module=' . $this->module->id . '&id_order=' . $this->module->currentOrder . '&key=' . $customer->secure_key);
    }

}
