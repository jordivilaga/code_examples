<?php

/**
 * @since 1.5.0
 */
class SagepayPaymentModuleFrontController extends ModuleFrontController {

    public $ssl = true;

    /**
     * @see FrontController::initContent()
     */
    public function initContent() {

        $this->display_column_left = false;
        parent::initContent();

        $cart = $this->context->cart;
        if (!$this->module->checkCurrency($cart))
            Tools::redirect('index.php?controller=order');

        $currency = new Currency((int) $cart->id_currency);
        $lang = new Language((int) $cart->id_lang);
        $customer = new Customer((int) $cart->id_customer);
        $invoiceAddress = new Address((int) $cart->id_address_invoice);
        $deliveryAddress = new Address((int) $cart->id_address_delivery);
        $invoiceCountry = new Country((int) $invoiceAddress->id_country, (int) $cart->id_lang);
        $deliveryCountry = new Country((int) $deliveryAddress->id_country, (int) $cart->id_lang);
        $invoiceState = (Validate::isLoadedObject($invoiceAddress) AND $invoiceAddress->id_state) ? new State(intval($invoiceAddress->id_state)) : false;
        $deliveryState = (Validate::isLoadedObject($deliveryAddress) AND $deliveryAddress->id_state) ? new State(intval($deliveryAddress->id_state)) : false;

        $strVendorTxCode = 'ice' . md5($cart->id . date("Y-m-d H:i:s"));
        $strToCrypt = 'VendorTxCode=' . $strVendorTxCode;
        //if (strlen($strPartnerID) > 0)
        //    $strToCrypt .=  "&ReferrerID=" . Configuration::get('SAGEPAY_PARTNERID');
        $strToCrypt .= '&Amount=' . number_format($cart->getOrderTotal(true, 3), 2, '.', ',');
        $strToCrypt .= '&Currency=' . $currency->iso_code;
        $strToCrypt .= '&Description=Payment with Sagepay: ' . $this->ensec(Configuration::get("PS_SHOP_NAME"));
        $strToCrypt .= '&SuccessURL='.$this->context->link->getModuleLink('sagepay', 'validation');
        $strToCrypt .= '&FailureURL='.$this->context->link->getModuleLink('sagepay', 'validation');
        $strToCrypt .= '&CustomerName=' . $this->ensec($customer->firstname . ' ' . $customer->lastname);
        $strToCrypt .= '&SendEMail=0';
        $strToCrypt .= '&BillingFirstnames=' . $this->ensec($customer->firstname);
        $strToCrypt .= '&BillingSurname=' . $this->ensec($customer->lastname);
        $strToCrypt .= '&BillingAddress1=' . $this->ensec($invoiceAddress->address1);
        $strToCrypt .= '&BillingAddress2=' . $this->ensec($invoiceAddress->address2);
        $strToCrypt .= '&BillingCity=' . $this->ensec($invoiceAddress->city);
        $strToCrypt .= '&BillingPostCode=' . $this->ensec($invoiceAddress->postcode);
        $strToCrypt .= '&BillingCountry=' . $invoiceCountry->iso_code;
        if (!empty($invoiceState))
            $strToCrypt .= '&BillingState=' . $invoiceState->iso_code;
        $strToCrypt .= '&BillingPhone=' . $invoiceAddress->phone;
        $strToCrypt .= '&DeliveryFirstnames=' . $this->ensec($customer->firstname);
        $strToCrypt .= '&DeliverySurname=' . $this->ensec($customer->lastname);
        $strToCrypt .= '&DeliveryAddress1=' . $this->ensec($deliveryAddress->address1);
        $strToCrypt .= '&DeliveryAddress2=' . $this->ensec($deliveryAddress->address2);
        $strToCrypt .= '&DeliveryCity=' . $this->ensec($deliveryAddress->city);
        $strToCrypt .= '&DeliveryPostCode=' . $this->ensec($deliveryAddress->postcode);
        $strToCrypt .= '&DeliveryCountry=' . $deliveryCountry->iso_code;
        if (!empty($deliveryState))
            $strToCrypt .= '&DeliveryState=' . $deliveryState->iso_code;
        $strToCrypt .= '&DeliveryPhone=' . $deliveryAddress->phone;

        $cryptedStr = $this->xor_crypt($strToCrypt, Configuration::get('SAGEPAY_CRYPTKEY'));
        $cryptedData = base64_encode($cryptedStr);

        $sagepayParams = array();
        $sagepayParams['VPSProtocol'] = '2.23';
        $sagepayParams['TxType'] = Configuration::get('SAGEPAY_3DSECURE') ? 'AUTHENTICATED' : 'DEFERRED';
        $sagepayParams['Vendor'] = $this->ensec(Configuration::get('SAGEPAY_ID'));
        $sagepayParams['Crypt'] = $cryptedData;

        if (Configuration::get('SAGEPAY_DEMO_MODE') == 0) {
            $url = "https://test.sagepay.com/gateway/service/vspform-register.vsp";
        } else {
            $url = "https://live.sagepay.com/gateway/service/vspform-register.vsp";
        }

        $this->context->smarty->assign(array(
            'nbProducts' => $cart->nbProducts(),
            'cust_currency' => $cart->id_currency,
            'currencies' => $this->module->getCurrency((int) $cart->id_currency),
            'total' => $cart->getOrderTotal(true, Cart::BOTH),
            'this_path' => $this->module->getPathUri(),
            'this_path_ssl' => Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . 'modules/' . $this->module->name . '/',
            'params' => $sagepayParams,
            'url' => $url
        ));

        $this->setTemplate('payment_execution.tpl');
    }

    public function ensec($input) {
        return htmlentities(utf8_encode(str_replace("&", "%26", $input)));
    }

    function to_array($input) {

        $values = array("Status", "StatusDetail", "VendorTxCode", "VPSTxId", "TxAuthNo", "Amount", "AVSCV2", "AddressResult", "PostCodeResult", "CV2Result", "GiftAid", "3DSecureStatus", "CAVV", "AddressStatus", "CardType", "Last4Digits", "PayerStatus", "CardType");

        $output = array();
        $resultArray = array();

        for ($i = count($values) - 1; $i >= 0; $i--) {
            $start = strpos($input, $values[$i]);
            if ($start !== false) {
                $resultArray[$i]->start = $start;
                $resultArray[$i]->token = $values[$i];
            }
        }

        sort($resultArray);
        for ($i = 0; $i < count($resultArray); $i++) {
            $valueStart = $resultArray[$i]->start + strlen($resultArray[$i]->token) + 1;
            if ($i == (count($resultArray) - 1)) {
                $output[$resultArray[$i]->token] = substr($input, $valueStart);
            } else {
                $valueLength = $resultArray[$i + 1]->start - $resultArray[$i]->start - strlen($resultArray[$i]->token) - 2;
                $output[$resultArray[$i]->token] = substr($input, $valueStart, $valueLength);
            }
        }
        return $output;
    }

    public function xor_crypt($input, $key) {
        $keyList = array();
        $output = "";
        for ($i = 0; $i < strlen($key); $i++) {
            $keyList[$i] = ord(substr($key, $i, 1));
        }
        for ($i = 0; $i < strlen($input); $i++) {
            if (strlen($key))
                $output .= chr(ord(substr($input, $i, 1)) ^ ($keyList[$i % strlen($key)]));
            else
                $output .= chr(ord(substr($input, $i, 1)) ^ 0);
        }
        return $output;
    }

}
