{if $status == 'ok'}
    <p>{l s='Your order on %s is complete.' sprintf=$shop_name mod='sagepay'}
        <br /><br />
        {l s='Please send us a Sagepay with:' mod='sagepay'}
        {if !isset($reference)}
            <br /><br />- {l s='Do not forget to insert your order number #%d in the subject of your Sagepay' sprintf=$id_order mod='sagepay'}
        {else}
            <br /><br />- {l s='Do not forget to insert your order reference %s in the subject of your Sagepay.' sprintf=$reference mod='sagepay'}
        {/if}		<br /><br />{l s='An e-mail has been sent to you with this information.' mod='sagepay'}
        <br /><br /> <strong>{l s='Your order will be sent as soon as we receive your settlement.' mod='sagepay'}</strong>
        <br /><br />{l s='For any questions or for further information, please contact our' mod='sagepay'} <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='sagepay'}</a>.
    </p>
{else}
    <p class="warning">
        {l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='sagepay'} 
        <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='sagepay'}</a>.
    </p>
{/if}
