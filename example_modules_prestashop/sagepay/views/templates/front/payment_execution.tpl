{capture name=path}{l s='Sagepay payment' mod='sagepay'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h2>{l s='Order summary' mod='sagepay'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
    <p class="warning">{l s='Your shopping cart is empty.' mod='sagepay'}</p>
{else}

    <h3>{l s='Sagepay payment' mod='sagepay'}</h3>
    <form action="{$url}" method="post">
        <p>
            <img src="{$this_path}sagepay.jpg" alt="{l s='Sagepay' mod='sagepay'}" width="86" height="49" style="float:left; margin: 0px 10px 5px 0px;" />
            {l s='You have chosen to pay by Sagepay.' mod='sagepay'}
            <br/><br />
            {l s='Here is a short summary of your order:' mod='sagepay'}
        </p>
        <p style="margin-top:20px;">
            {l s='- The total amount of your order is' mod='sagepay'}
            <span id="amount" class="price">{displayPrice price=$total}</span>
            {if $use_taxes == 1}
                {l s='(tax incl.)' mod='sagepay'}
            {/if}
        </p>
        <p>
            {if $currencies|@count > 1}
                {l s='- We accept several currencies to be sent by Sagepay.' mod='sagepay'}
                <br /><br />
                {l s='Please select one of the following currencies:' mod='sagepay'}
                <select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
                    {foreach from=$currencies item=currency}
                        <option value="{$currency.id_currency}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name}</option>
                    {/foreach}
                </select>
            {else}
                {l s='We accept the following currency to be sent by Sagepay:' mod='sagepay'}&nbsp;<b>{$currencies.0.name}</b>
                <input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
            {/if}
        </p>
        <p>
            {l s='You will be re-directed to Sagepay\'s payment gateway once you confirm your order.' mod='sagepay'}
            <br /><br />
            <b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='sagepay'}.</b>
        </p>
        <p class="cart_navigation">
            <input type="submit" name="submit" value="{l s='I confirm my order' mod='sagepay'}" class="exclusive_large" />
            <a href="{$link->getPageLink('order', true, NULL, "step=3")}" class="button_large" target="_blank">{l s='Other payment methods' mod='sagepay'}</a>
        </p>
        
        {foreach from=$params key=k item=v}
            <input type="hidden" name="{$k}" value="{$v}" />
        {/foreach}
    </form>
{/if}
